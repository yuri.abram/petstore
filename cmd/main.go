package main

import (
	"github.com/joho/godotenv"
	"gitlab/petstore/config"
	_ "gitlab/petstore/docs"
	"gitlab/petstore/internal/infrastructure/logs"
	"gitlab/petstore/run"
	"os"
)

// @title задача Реализация handlers Petstore task3.3.4.2
// @version 2.0
// @description  Petstore
// @host localhost:8080
// @BasePath /

//	@securityDefinitions.apikey	Bearer
//	@in							header
//	@name						Authorization
//	@description The following syntax must be used in the 'Authorization' header: Bearer: xxxxxx.yyyyyyy.zzzzzz

func main() {

	// Загружаем переменные окружения из файла .env
	err := godotenv.Load()
	// Создаем конфигурацию приложения
	conf := config.NewAppConf()
	// Создаем логгер
	logger := logs.NewLogger(conf, os.Stdout)
	if err != nil {
		logger.Fatal("error loading .env file")
	}
	// Инициализируем конфигурацию приложения с логгером
	conf.Init(logger)
	// Создаем инстанс приложения
	app := run.NewApp(conf, logger)

	exitCode := app.
		// Инициализируем приложение
		Bootstrap().
		// Запускаем приложение
		Run()
	os.Exit(exitCode)
}
