package router

import (
	"github.com/go-chi/chi/v5"
	chimw "github.com/go-chi/chi/v5/middleware"
	httpSwagger "github.com/swaggo/http-swagger"
	_ "gitlab/petstore/docs"
	"gitlab/petstore/internal/infrastructure/component"
	"gitlab/petstore/internal/infrastructure/middleware"
	"gitlab/petstore/internal/modules"
	"gitlab/petstore/internal/router"
	"net/http"
)

func NewRouter(controllers *modules.Controllers, components *component.Components) *chi.Mux {
	r := chi.NewRouter()
	setBasicMiddlewares(r)
	//setDefaultRoutes(r)

	r.Mount("/", router.NewApiRouter(controllers, components))
	return r
}

func setBasicMiddlewares(r *chi.Mux) {
	proxy := middleware.NewReverseProxy()
	r.Use(chimw.Recoverer)
	r.Use(proxy.ReverseProxy)
	r.Use(chimw.Logger)

	r.Use(chimw.RealIP)
	r.Use(chimw.RequestID)
}

func setDefaultRoutes(r *chi.Mux) {
	r.Get("/swagger/*", httpSwagger.Handler(httpSwagger.URL("http://localhost:8080/swagger/doc.json")))
	r.Get("/static/*", func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/static/", http.FileServer(http.Dir("./static"))).ServeHTTP(w, r)
	})
}
