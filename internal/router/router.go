package router

import (
	"github.com/go-chi/chi"
	"github.com/go-chi/jwtauth/v5"
	httpSwagger "github.com/swaggo/http-swagger"
	_ "gitlab/petstore/docs"
	"gitlab/petstore/internal/infrastructure/component"
	"gitlab/petstore/internal/modules"
	"gitlab/petstore/internal/modules/user/storage"
	"net/http"
)

func NewApiRouter(controllers *modules.Controllers, components *component.Components) http.Handler {
	r := chi.NewRouter()
	user := controllers.User
	pet := controllers.Pet
	store := controllers.Store

	r.Get("/swagger/*", httpSwagger.Handler(
		httpSwagger.URL("http://localhost:8080/swagger/doc.json"),
	))

	// pet, all routes are private
	r.Group(func(r chi.Router) {
		r.Use(jwtauth.Verifier(storage.TokenAuth))
		r.Use(jwtauth.Authenticator(storage.TokenAuth))

		r.Route("/pet", func(r chi.Router) {

			r.Post("/{petId}/uploadImage", pet.UploadImage)
			r.Post("/", pet.AddPet)
			r.Put("/", pet.UpdatePet)
			r.Get("/findByStatus", pet.FindPetStatus)
			r.Post("/{petId}", pet.FindPetId)
			r.Post("/{petId}", pet.UpdatePet)
			r.Delete("/{petId}", pet.DeletePet)
		})
	})

	// store
	r.Group(func(r chi.Router) {
		r.Route("/store", func(r chi.Router) {
			// public part
			r.Group(func(r chi.Router) {
				r.Post("/order", store.CreateOrder)
				r.Get("/order/{orderId}", store.GetOrderById)
				r.Delete("/order/{orderId}", store.DeleteOrder)
			})

			// private part
			r.Group(func(r chi.Router) {
				r.Use(jwtauth.Verifier(storage.TokenAuth))
				r.Use(jwtauth.Authenticator(storage.TokenAuth))

				r.Get("/inventory", store.GetInventory)
			})
		})
	})

	// user, all routes are public
	r.Group(func(r chi.Router) {
		r.Route("/user", func(r chi.Router) {
			r.Post("/createWithList", user.CreateUserWithList)
			r.Get("/{username}", user.GetByName)
			r.Put("/{username}", user.UpdateUser)
			r.Delete("/{username}", user.DeleteUser)
			r.Get("/", user.UserLogin)
			r.Get("/logout", user.UserLogout)
			// r.Post("/createWithArray", user.CreateUserWithArray) // not implemented, same as createWithList
			r.Post("/", user.CreateUser)
		})
	})

	return r
}
