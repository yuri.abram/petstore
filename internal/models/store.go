package models

import "time"

type Order struct {
	ID       int64     `json:"id" db:"id" db_type:"SERIAL PRIMARY KEY" db_default:"default null" db_ops:"create,update"`
	PetID    int64     `json:"petId" db:"pet_id" db_default:"default null" db_ops:"create,update"`
	Quantity int       `json:"quantity" db:"quantity" db_default:"default null" db_ops:"create,update"`
	ShipDate time.Time `json:"shipDate" db:"ship_date" db_default:"default null" db_ops:"create,update"`
	Status   string    `json:"status" db:"status" example:"placed" db_default:"default null" db_ops:"create,update"`
	Complete bool      `json:"complete" db:"complete" db_default:"default false" db_ops:"create,update"`
}

func (o *Order) TableName() string {
	return "order"
}

func (o *Order) OnCreate() []string {
	return []string{}
}

func (o *Order) GetID() int {
	return int(o.ID)
}

func (o *Order) SetID(id int) {
	o.ID = int64(id)
}

func (o *Order) GetStatus() string {
	return o.Status
}

func (o *Order) SetStatus(status string) {
	o.Status = status
}

func (o *Order) GetQuantity() int {
	return o.Quantity
}

func (o *Order) SetQuantity(quantity int) {
	o.Quantity = quantity
}
