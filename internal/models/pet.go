package models

type PhotoUrls struct {
	Names []string `json:"names,omitempty" db_default:"default null" db_ops:"create,update"`
	Urls  []string `json:"urls,omitempty" db_default:"default null" db_ops:"create,update"`
}

type Category struct {
	ID   int64  `json:"id" db:"id" db_type:"SERIAL PRIMARY KEY" db_default:"default null" db_ops:"create,update"`
	Name string `json:"name" db:"name" db_type:"text" example:"dogs" db_default:"default null" db_ops:"create,update"`
}
type Tag struct {
	ID   int64  `json:"id" db:"id" db_type:"SERIAL PRIMARY KEY" db_default:"default null" db_ops:"create,update"`
	Name string `json:"name" db:"name" db_type:"text" db_default:"default null" db_ops:"create,update"`
}

type Pet struct {
	ID        int64     `json:"id" db:"id" db_type:"SERIAL PRIMARY KEY" db_default:"default null" db_ops:"create,update"`
	Category  Category  `json:"category" db:"category" db_default:"default null" db_ops:"create,update"`
	Name      string    `json:"name" db:"name" example:"example_pet_name" db_default:"default null" db_ops:"create,update"`
	PhotoUrls PhotoUrls `json:"photoUrls" db:"photoUrls" db_default:"default null" db_ops:"create,update"`
	Tags      []Tag     `json:"tags" db:"tags" db_default:"default null" db_ops:"create,update"`
	Status    string    `json:"status" db:"status" db_default:"default null" db_ops:"create,update"`
}

func (p *Pet) TableName() string {
	return "pet"
}

func (p *Pet) OnCreate() []string {
	return []string{}
}

func (p *Pet) GetID() int {
	return int(p.ID)
}

func (p *Pet) SetID(id int) {
	p.ID = int64(id)
}

func (p *Pet) GetStatus() string {
	return p.Status
}

func (p *Pet) SetStatus(status string) {
	p.Status = status
}
