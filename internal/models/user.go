package models

type User struct {
	ID         int    `json:"id" db:"id" db_type:"BIGSERIAL primary key" db_default:"not null"`
	Username   string `json:"username" db:"username" db_type:"VARCHAR(100)" db_default:"NULL" db_ops:"create,update" example:"johndoe"`
	FirstName  string `json:"firstName" db:"first_name" db_type:"VARCHAR(100)" db_default:"NULL" db_ops:"create,update" example:"John"`
	LastName   string `json:"lastName" db:"last_name" db_type:"VARCHAR(100)" db_default:"NULL" db_ops:"create,update" example:"Doe"`
	Email      string `json:"email" db:"email" db_type:"VARCHAR(100)" db_default:"NULL" db_ops:"create,update" example:"john.doe@example.com"`
	Password   string `json:"password" db:"password" db_type:"VARCHAR(100)" db_default:"NULL" db_ops:"create,update" example:"123456"`
	Phone      string `json:"phone" db:"phone" db_type:"VARCHAR(100)" db_default:"NULL" db_ops:"create,update" example:"1234567890"`
	UserStatus int    `json:"userStatus" db:"user_status" db_type:"INT" db_default:"NULL" db_ops:"create,update" example:"1"`
}

func (u *User) OnCreate() []string {
	return []string{}
}

func (u *User) TableName() string {
	return "users"
}

func (u *User) GetID() int {
	return u.ID
}
func (u *User) GetUsername() string {
	return u.Username
}
func (u *User) GetFirstName() string {
	return u.FirstName
}
func (u *User) GetLastName() string {
	return u.LastName
}
func (u *User) GetEmail() string {
	return u.Email
}
func (u *User) GetPassword() string {
	return u.Password
}
func (u *User) GetPhone() string {
	return u.Phone
}
func (u *User) GetUserStatus() int {
	return u.UserStatus
}

func (u *User) SetID(id int) {
	u.ID = id
}
func (u *User) SetUsername(username string) {
	u.Username = username
}
func (u *User) SetFirstName(firstname string) {
	u.FirstName = firstname
}
func (u *User) SetLastName(lastname string) {
	u.LastName = lastname
}
func (u *User) SetEmail(email string) {
	u.Email = email
}
func (u *User) SetPassword(password string) {
	u.Password = password
}
func (u *User) SetPhone(phone string) {
	u.Phone = phone
}
func (u *User) SetUserStatus(status int) {
	u.UserStatus = status
}
