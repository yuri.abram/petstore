package modules

import (
	"gitlab/petstore/internal/infrastructure/component"
	pserv "gitlab/petstore/internal/modules/pet/service"
	stserv "gitlab/petstore/internal/modules/store/service"
	userv "gitlab/petstore/internal/modules/user/service"
)

type Services struct {
	PetService   pserv.PetServicer
	StoreService stserv.StoreServicer
	UserService  userv.UserServicer
}

func NewServices(storage *Storages, components *component.Components) *Services {
	return &Services{
		PetService:   pserv.NewPetService(storage.Pet, components.Logger),
		StoreService: stserv.NewStoreService(storage.Store, components.Logger),
		UserService:  userv.NewUserService(storage.Users, components.Logger),
	}
}
