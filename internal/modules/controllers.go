package modules

import (
	"gitlab/petstore/internal/infrastructure/component"
	pcontroller "gitlab/petstore/internal/modules/pet/controller"
	scontroller "gitlab/petstore/internal/modules/store/controller"
	ucontroller "gitlab/petstore/internal/modules/user/controller"
)

type Controllers struct {
	User  ucontroller.UserController
	Pet   pcontroller.PetController
	Store scontroller.StoreController
}

func NewControllers(services *Services, components *component.Components) *Controllers {

	return &Controllers{
		User:  ucontroller.NewUserCtl(services.UserService, components),
		Pet:   pcontroller.NewPetCtl(services.PetService, components),
		Store: scontroller.NewStoreCtl(services.StoreService, components),
	}
}
