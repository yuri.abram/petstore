package storage

import (
	"context"
	sq "github.com/Masterminds/squirrel"
	"gitlab/petstore/internal/db/adapter"
	"gitlab/petstore/internal/infrastructure/db/scanner"
	"gitlab/petstore/internal/models"
)

type PetStorager interface {
	Create(ctx context.Context, pet models.Pet) error
	Update(ctx context.Context, pet models.Pet) error
	Delete(ctx context.Context, petID int) error
	GetByID(ctx context.Context, petID int) (models.Pet, error)
	List(ctx context.Context, condition adapter.Condition) ([]models.Pet, int, error)
}

type Pet struct {
	adapter *adapter.SQLAdapter
}

func NewPetStorage(sqlAdapter *adapter.SQLAdapter) *Pet {
	return &Pet{adapter: sqlAdapter}
}

func (p *Pet) Create(ctx context.Context, pet models.Pet) error {
	return p.adapter.Create(ctx, &pet)
}

func (p *Pet) Update(ctx context.Context, pet models.Pet) error {
	err := p.adapter.Update(ctx, &pet, adapter.Condition{
		Equal: sq.Eq{
			"id": pet.GetID(),
		},
	}, scanner.Update)
	if err != nil {
		return err
	}
	return nil
}

func (p *Pet) Delete(ctx context.Context, petID int) error {
	pet, err := p.GetByID(ctx, petID)
	if err != nil {
		return err
	}

	return p.adapter.Delete(ctx, &pet, adapter.Condition{
		Equal: sq.Eq{
			"id": petID,
		},
	})
}

func (p *Pet) GetByID(ctx context.Context, petID int) (models.Pet, error) {
	var pet models.Pet
	err := p.adapter.List(ctx, &pet, "pets", adapter.Condition{
		Equal: sq.Eq{
			"id": petID,
		},
	})
	if err != nil {
		return models.Pet{}, err
	}
	return pet, nil
}

func (p *Pet) List(ctx context.Context, condition adapter.Condition) ([]models.Pet, int, error) {
	var pets []models.Pet
	var count int
	err := p.adapter.List(ctx, &pets, "pets", condition)
	if err != nil {
		return nil, 0, err
	}

	return pets, count, nil
}
