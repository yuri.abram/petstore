package controller

import (
	"encoding/json"
	sq "github.com/Masterminds/squirrel"
	"gitlab/petstore/internal/db/adapter"
	"gitlab/petstore/internal/infrastructure/component"
	"gitlab/petstore/internal/infrastructure/responder"
	"gitlab/petstore/internal/models"
	"gitlab/petstore/internal/modules/pet/service"
	"net/http"
)

type PetController interface {
	UploadImage(w http.ResponseWriter, r *http.Request)
	AddPet(w http.ResponseWriter, r *http.Request)
	UpdatePet(w http.ResponseWriter, r *http.Request)
	FindPetStatus(w http.ResponseWriter, r *http.Request)
	FindPetId(w http.ResponseWriter, r *http.Request)
	DeletePet(w http.ResponseWriter, r *http.Request)
}

type Pet struct {
	service   service.PetServicer
	responder responder.Responder
}

func NewPetCtl(service service.PetServicer, components *component.Components) *Pet {
	return &Pet{
		service:   service,
		responder: components.Responder,
	}
}

// UploadImage
// @Summary Uploads an image
// @Tags Pet
// @Accept  json
// @Produce  json
// @Param request body ImageUploadRq true "Upload image request"
// @Success 200 {object} ApiResponse
// @Router /pet/{petId}/uploadImage [post]
func (p Pet) UploadImage(w http.ResponseWriter, r *http.Request) {
	var rq ImageUploadRq
	err := json.NewDecoder(r.Body).Decode(&rq)
	if err != nil {
		p.responder.ErrorBadRequest(w, err)
		return
	}
	err = p.service.AddImage(r.Context(), int(rq.ID), rq.ImageName, rq.ImagePath)

	if err != nil {
		p.responder.OutputJSON(w, ApiResponse{
			Code:    http.StatusInternalServerError,
			Type:    "error",
			Message: err.Error(),
		})
	}
	p.responder.OutputJSON(w, ApiResponse{
		Code:    http.StatusOK,
		Type:    "success",
		Message: "image uploaded",
	})
}

// AddPet
// @Summary Add a new pet to the store
// @Tags Pet
// @Accept  json
// @Produce  json
// @Param request body Pet true "Add pet request"
// @Success 200 {object} ApiResponse
// @Failure 405 {object} ApiResponse
// @Router /pet [post]
func (p Pet) AddPet(w http.ResponseWriter, r *http.Request) {
	var pet AddPetRq
	err := json.NewDecoder(r.Body).Decode(&pet)
	if err != nil {
		p.responder.ErrorBadRequest(w, err)
		return
	}

	err = p.service.Create(r.Context(), models.Pet(pet))
	if err != nil {
		p.responder.ErrorInternal(w, err)
		return
	}

	p.responder.OutputJSON(w, ApiResponse{
		Code:    http.StatusOK,
		Type:    "success",
		Message: "pet added",
	})
}

// UpdatePet
// @Summary Update an existing pet
// @Tags Pet
// @Accept  json
// @Produce  json
// @Param request body UpdatePetRq true "Update pet request"
// @Success 200 {object} ApiResponse
// @Failure 400 Bad Request
// @Failure 500 Internal Server Error
// @Router /pet [put]
func (p Pet) UpdatePet(w http.ResponseWriter, r *http.Request) {
	var pet UpdatePetRq
	err := json.NewDecoder(r.Body).Decode(&pet)
	if err != nil {
		p.responder.ErrorBadRequest(w, err)
		return
	}
	err = p.service.Update(r.Context(), models.Pet(pet))
	if err != nil {
		p.responder.ErrorInternal(w, err)
		return
	}
	p.responder.OutputJSON(w, ApiResponse{
		Code:    http.StatusOK,
		Type:    "success",
		Message: "pet updated",
	})
}

// FindPetStatus
// @Summary Finds Pets by status
// @Tags Pet
// @Accept  json
// @Produce  json
// @Param request query ByStatusRq true "Find pet by status request"
// @Success 200 {object} ByStatusRsp
// @Failure 400 Bad Request
// @Router /pet/findByStatus [get]
func (p Pet) FindPetStatus(w http.ResponseWriter, r *http.Request) {
	var pet ByStatusRq
	err := json.NewDecoder(r.Body).Decode(&pet)
	if err != nil {
		p.responder.ErrorBadRequest(w, err)
		return
	}
	pets, _, err := p.service.List(r.Context(), adapter.Condition{
		Equal: sq.Eq{
			"status": pet.Status,
		},
	})

	if err != nil {
		p.responder.ErrorInternal(w, err)
		return
	}
	p.responder.OutputJSON(w, ByStatusRsp(pets))

}

// FindPetId
// @Summary Finds pet by ID
// @Tags Pet
// @Accept  json
// @Produce  json
// @Param request query ByIDRq true "Find pet by ID request"
// @Success 200 {object} Pet
// @Failure 400 Bad Request
// @Router /pet/{petId} [get]
func (p Pet) FindPetId(w http.ResponseWriter, r *http.Request) {
	var id ByIDRq
	err := json.NewDecoder(r.Body).Decode(&id)
	if err != nil {
		p.responder.ErrorBadRequest(w, err)
		return
	}

	pet, err := p.service.GetByID(r.Context(), int(id.ID))
	if err != nil {
		p.responder.ErrorInternal(w, err)
		return
	}
	p.responder.OutputJSON(w, pet)

}

// DeletePet
// @Summary Deletes a pet
// @Tags Pet
// @Accept  json
// @Produce  json
// @Param request query ByIDRq true "Delete pet request"
// @Success 200 {object} ApiResponse
// @Failure 400 Bad Request
// @Failure 500 Internal Server Error
// @Router /pet [delete]
func (p Pet) DeletePet(w http.ResponseWriter, r *http.Request) {
	var id ByIDRq
	err := json.NewDecoder(r.Body).Decode(&id)
	if err != nil {
		p.responder.ErrorBadRequest(w, err)
		return
	}

	err = p.service.Delete(r.Context(), int(id.ID))
	if err != nil {
		p.responder.ErrorInternal(w, err)
		return
	}

	p.responder.OutputJSON(w, ApiResponse{
		Code:    http.StatusOK,
		Type:    "success",
		Message: "pet deleted",
	})
}
