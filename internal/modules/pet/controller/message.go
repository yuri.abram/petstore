package controller

import "gitlab/petstore/internal/models"

type ApiResponse struct {
	Code    int    `json:"code"`
	Type    string `json:"type"`
	Message string `json:"message"`
}

type ImageUploadRq struct {
	ID        int64  `json:"id"`
	ImageName string `json:"image_name"`
	ImagePath string `json:"image"`
}

type ByStatusRq struct {
	Status string `json:"status"`
}

type ByIDRq struct {
	ID int64 `json:"id"`
}

type FormUpdateRq struct {
	ID     int64  `json:"id"`
	Name   string `json:"name"`
	Status string `json:"status"`
}

type AddPetRq models.Pet
type UpdatePetRq models.Pet

type ByStatusRsp []models.Pet
type ByIDRsp models.Pet
