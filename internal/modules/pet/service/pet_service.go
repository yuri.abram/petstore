package service

import (
	"context"
	"gitlab/petstore/internal/db/adapter"
	"gitlab/petstore/internal/models"
	"gitlab/petstore/internal/modules/pet/storage"
	"go.uber.org/zap"
)

type PetServicer interface {
	Create(ctx context.Context, pet models.Pet) error
	Update(ctx context.Context, pet models.Pet) error
	Delete(ctx context.Context, petID int) error
	GetByID(ctx context.Context, petID int) (models.Pet, error)
	List(ctx context.Context, condition adapter.Condition) ([]models.Pet, int, error)
	AddImage(ctx context.Context, petId int, name, link string) error
}

type Pet struct {
	storage storage.PetStorager
	logger  *zap.Logger
}

func NewPetService(storage storage.PetStorager, logger *zap.Logger) *Pet {
	return &Pet{
		storage: storage,
		logger:  logger,
	}
}

func (p *Pet) Create(ctx context.Context, pet models.Pet) error {
	return p.storage.Create(ctx, pet)
}
func (p *Pet) Update(ctx context.Context, pet models.Pet) error {
	return p.storage.Update(ctx, pet)
}
func (p *Pet) Delete(ctx context.Context, petID int) error {
	return p.storage.Delete(ctx, petID)
}
func (p *Pet) GetByID(ctx context.Context, petID int) (models.Pet, error) {
	return p.storage.GetByID(ctx, petID)
}
func (p *Pet) List(ctx context.Context, condition adapter.Condition) ([]models.Pet, int, error) {
	return p.storage.List(ctx, condition)
}

func (p *Pet) AddImage(ctx context.Context, petId int, name, link string) error {
	pet, err := p.GetByID(ctx, petId)
	if err != nil {
		return err
	}

	pet.PhotoUrls.Names = append(pet.PhotoUrls.Names, name)
	pet.PhotoUrls.Urls = append(pet.PhotoUrls.Urls, link)

	err = p.Update(ctx, pet)
	if err != nil {
		return err
	}

	return nil
}
