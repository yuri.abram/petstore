package service

import (
	"context"
	"gitlab/petstore/internal/db/adapter"
	"gitlab/petstore/internal/models"
	"gitlab/petstore/internal/modules/user/storage"
	"go.uber.org/zap"
	"strconv"
)

type UserServicer interface {
	Create(ctx context.Context, u models.User) error
	Update(ctx context.Context, u models.User) error
	List(ctx context.Context, condition adapter.Condition) ([]models.User, int, error)
	Delete(ctx context.Context, userID string) error
	GetByID(ctx context.Context, userID string) (models.User, error)
	CreateWithList(ctx context.Context, users []models.User) error
	GetByName(ctx context.Context, username string) (models.User, error)
	Login(ctx context.Context, username, password string) (string, error)
	Logout(ctx context.Context, remote string) error
}

type UserService struct {
	storage storage.UserRepository
	logger  *zap.Logger
}

func NewUserService(storage storage.UserRepository, logger *zap.Logger) *UserService {
	return &UserService{
		storage: storage,
		logger:  logger,
	}
}

func (us *UserService) GetByName(ctx context.Context, username string) (models.User, error) {
	user, err := us.storage.List(ctx, adapter.Condition{
		Equal: map[string]interface{}{
			"username": username,
		},
	})
	if err != nil {
		us.logger.Error("failed to get user", zap.Error(err))
		return models.User{}, err
	}
	return user[0], nil
}

func (us *UserService) CreateWithList(ctx context.Context, users []models.User) error {
	for _, u := range users {
		err := us.storage.Create(ctx, u)
		if err != nil {
			us.logger.Error("failed to create user", zap.Error(err))
			return err
		}
	}
	return nil
}

func (us *UserService) Create(ctx context.Context, u models.User) error {
	err := us.storage.Create(ctx, u)
	if err != nil {
		us.logger.Error("failed to create user", zap.Error(err))
		return err
	}
	return nil
}

func (us *UserService) Update(ctx context.Context, u models.User) error {
	err := us.storage.Update(ctx, u)
	if err != nil {
		us.logger.Error("failed to update user", zap.Error(err))
		return err
	}
	return nil
}

func (us *UserService) List(ctx context.Context, condition adapter.Condition) ([]models.User, int, error) {
	users, err := us.storage.List(ctx, condition)
	if err != nil {
		us.logger.Error("failed to get users", zap.Error(err))
		return nil, 0, err
	}
	return users, len(users), nil
}

func (us *UserService) Delete(ctx context.Context, userID string) error {

	err := us.storage.Delete(ctx, userID)
	if err != nil {
		us.logger.Error("failed to delete user", zap.Error(err))
		return err
	}
	return nil
}

func (us *UserService) GetByID(ctx context.Context, userID string) (models.User, error) {
	userIDInt, err := strconv.Atoi(userID)
	if err != nil {
		us.logger.Error("failed to convert userID", zap.Error(err))
		return models.User{}, err
	}
	user, err := us.storage.GetByID(ctx, userIDInt)
	if err != nil {
		us.logger.Error("failed to get user", zap.Error(err))
		return models.User{}, err
	}
	return user, nil
}

func (us *UserService) Login(ctx context.Context, username, password string) (string, error) {
	if _, err := us.GetByName(ctx, username); err != nil {
		return "", err
	}

	token, err := us.storage.Login(ctx, username, password)
	if err != nil {
		return "", err
	}

	return token, nil
}

func (us *UserService) Logout(ctx context.Context, remote string) error {
	return us.storage.Logout(ctx, remote)
}
