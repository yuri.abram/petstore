package controller

import (
	"encoding/json"
	"fmt"
	"github.com/go-chi/chi/v5"
	"gitlab/petstore/internal/infrastructure/component"
	"gitlab/petstore/internal/infrastructure/responder"
	"gitlab/petstore/internal/modules/user/service"
	"net/http"
	"net/url"
)

type UserController interface {
	CreateUser(w http.ResponseWriter, r *http.Request)
	UpdateUser(w http.ResponseWriter, r *http.Request)
	DeleteUser(w http.ResponseWriter, r *http.Request)
	CreateUserWithList(w http.ResponseWriter, r *http.Request)
	GetByName(w http.ResponseWriter, r *http.Request)
	UserLogin(w http.ResponseWriter, r *http.Request)
	UserLogout(w http.ResponseWriter, r *http.Request)
}

type User struct {
	service   service.UserServicer
	responder responder.Responder
}

func NewUserCtl(service service.UserServicer, components *component.Components) *User {
	return &User{
		service:   service,
		responder: components.Responder,
	}
}

// UserLogin - login user
// @summary 		Логин пользователя
// @description 	Логин пользователя.
// @Tags 			User
// @Accept 			plain
// @Produce 		json
// @Param 			username formData string true "Username"
// @Param 			password formData string true "User password"
// @Success 200 {object} LoginResponse "User logged in"
// @Failure 400 {string} string "Bad request"
// @Failure 401 {string} string "Invalid credentials"
// @Router /user/login [get]
func (u *User) UserLogin(w http.ResponseWriter, r *http.Request) {
	url, err := url.Parse(r.URL.String())
	if err != nil {
		u.responder.ErrorBadRequest(w, fmt.Errorf("bad url"))
		return
	}
	queryData := url.Query()
	username := queryData.Get("username")
	password := queryData.Get("password")
	if username == "" || password == "" {
		u.responder.ErrorBadRequest(w, fmt.Errorf("bad username or password"))
		return
	}
	tokenString, err := u.service.Login(r.Context(), username, password)
	if err != nil {
		u.responder.ErrorInternal(w, err)
		return
	}

	u.responder.OutputJSON(w, LoginResponse{
		Success: true,
		Message: "Login successful",
		Token:   tokenString,
	})
}

func (u *User) UserLogout(w http.ResponseWriter, r *http.Request) {

	err := u.service.Logout(r.Context(), r.RemoteAddr)
	if err != nil {
		u.responder.ErrorInternal(w, err)
		return
	}
	u.responder.OutputJSON(w, BasicResponse{
		Success: true,
		Message: "Logout successful",
	})
}

// CreateUserWithList
// @Summary Create new user with list
// @Tags User
// @Accept json
// @Produce json
// @Param request body CreateListRequest true "Create user request"
// @Success 200 {object} BasicResponse
// @Failure 400 {object} BasicResponse
// @Router /user/createWithList [post]
func (u *User) CreateUserWithList(w http.ResponseWriter, r *http.Request) {
	var rq CreateListRequest
	err := json.NewDecoder(r.Body).Decode(&rq)
	if err != nil {
		u.responder.ErrorBadRequest(w, err)
		return
	}
	err = u.service.CreateWithList(r.Context(), rq)
	if err != nil {
		u.responder.ErrorInternal(w, err)
		return
	}
	u.responder.OutputJSON(w, BasicResponse{
		Success: true,
		Message: "users created",
	})
}

// GetByName
// @Summary Get user
// @Tags User
// @Accept plain
// @Produce json
// @Param username path string true "Get user request"
// @Success 200 {object} GetByIDResponse
// @Failure 400 {object} BasicResponse
// @Router /user/{username} [get]
func (u *User) GetByName(w http.ResponseWriter, r *http.Request) {
	username := chi.URLParam(r, "username")

	user, err := u.service.GetByName(r.Context(), username)
	if err != nil {
		u.responder.ErrorInternal(w, err)
		return
	}
	u.responder.OutputJSON(w, GetByIDResponse{
		Success: true,
		Message: "user has been found",
		Data:    user,
	})
}

// CreateUser
// @Summary Create new user
// @Tags User
// @Accept json
// @Produce json
// @Param request body BasicRequest true "Create user request"
// @Success 200 {object} BasicResponse
// @Failure 400 {object} BasicResponse
// @Router /user [post]
func (u *User) CreateUser(w http.ResponseWriter, r *http.Request) {
	var rq BasicRequest
	err := json.NewDecoder(r.Body).Decode(&rq)
	if err != nil {
		u.responder.ErrorBadRequest(w, err)
		return
	}
	err = u.service.Create(r.Context(), rq.User)
	if err != nil {
		u.responder.ErrorInternal(w, err)
		return
	}
	u.responder.OutputJSON(w, BasicResponse{
		Success: true,
		Message: "user created",
	})
}

// UpdateUser
// @Summary Update user
// @Tags User
// @Accept json
// @Produce json
// @Param username path string true "Update user request"
// @Param request body BasicRequest true "Update user request"
// @Success 200 {object} BasicResponse
// @Failure 400 {object} BasicResponse
// @Router /user/{username} [put]
func (u *User) UpdateUser(w http.ResponseWriter, r *http.Request) {
	username := chi.URLParam(r, "username")
	var rq BasicRequest
	err := json.NewDecoder(r.Body).Decode(&rq)
	if err != nil {
		u.responder.ErrorBadRequest(w, err)
		return
	}
	if username != rq.User.Username {
		u.responder.ErrorBadRequest(w, fmt.Errorf("username not match"))
		return
	}
	err = u.service.Update(r.Context(), rq.User)
	if err != nil {
		u.responder.ErrorInternal(w, err)
		return
	}
	u.responder.OutputJSON(w, BasicResponse{
		Success: true,
		Message: "user was updated",
	})

}

// DeleteUser
// @Summary Delete user
// @Tags User
// @Produce json
// @Param username path string true "Delete user request"
// @Success 200 {object} BasicResponse
// @Failure 400 {object} BasicResponse
// @Failure 500 {object} BasicResponse
// @Router /user/{username} [delete]
func (u *User) DeleteUser(w http.ResponseWriter, r *http.Request) {
	username := chi.URLParam(r, "username")

	err := u.service.Delete(r.Context(), username)
	if err != nil {
		u.responder.ErrorInternal(w, err)
		return
	}
	u.responder.OutputJSON(w, BasicResponse{
		Success: true,
		Message: "user is now deleted",
	})
}
