package controller

import "gitlab/petstore/internal/models"

type RegisterRequest struct {
	Email          string `json:"email"`
	Password       string `json:"password"`
	RetypePassword string `json:"retype_password"`
	IdempotencyKey string `json:"idempotency_key"`
}

type Data struct {
	User models.User `json:"user,omitempty"`
}

type BasicRequest struct {
	User models.User `json:"user,omitempty"`
}

type IDRequest struct {
	UserID string `json:"id"`
}

type ListRequest struct {
	Limit  int64 `json:"limit"`
	Offset int64 `json:"offset"`
}

type ListResponse struct {
	Success bool          `json:"success"`
	Message string        `json:"message"`
	Data    []models.User `json:"data"`
}

type GetByIDResponse struct {
	Success bool        `json:"success"`
	Message string      `json:"message"`
	Data    models.User `json:"data"`
}

type BasicResponse struct {
	Success bool   `json:"success"`
	Message string `json:"message"`
}

type CreateListRequest []models.User

type LoginRequest struct {
	Email    string `json:"email" example:"admin"`
	Password string `json:"password" example:"pass"`
}

type LoginResponse struct {
	Success bool   `json:"success"`
	Message string `json:"message"`
	Token   string `json:"token"`
}
