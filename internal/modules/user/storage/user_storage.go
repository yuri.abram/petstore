package storage

import (
	"context"
	"fmt"
	sq "github.com/Masterminds/squirrel"
	"github.com/go-chi/jwtauth/v5"
	"gitlab/petstore/internal/db/adapter"
	"gitlab/petstore/internal/infrastructure/db/scanner"
	"gitlab/petstore/internal/models"
)

type UserRepository interface {
	Create(ctx context.Context, u models.User) error
	Update(ctx context.Context, u models.User) error
	GetByID(ctx context.Context, userID int) (models.User, error)
	List(ctx context.Context, condition adapter.Condition) ([]models.User, error)
	Delete(ctx context.Context, username string) error
	Login(ctx context.Context, username, password string) (string, error)
	Logout(ctx context.Context, remote string) error
}

type UserStorage struct {
	adapter *adapter.SQLAdapter
}

func NewUserStorage(sqlAdapter *adapter.SQLAdapter) *UserStorage {
	return &UserStorage{adapter: sqlAdapter}
}

func (us *UserStorage) Create(ctx context.Context, u models.User) error {
	return us.adapter.Create(ctx, &u)
}

// Update - обновление пользователя в БД
func (us *UserStorage) Update(ctx context.Context, u models.User) error {
	err := us.adapter.Update(ctx, &u, adapter.Condition{
		Equal: sq.Eq{
			"id": u.GetID(),
		},
	}, scanner.Update)
	if err != nil {
		return err
	}
	return nil
}

func (us *UserStorage) GetByID(ctx context.Context, userID int) (models.User, error) {
	var dto models.User
	var err error

	// если данных нет в кеше, то получаем их из БД
	var list []models.User
	err = us.adapter.List(ctx, &list, dto.TableName(), adapter.Condition{
		Equal: map[string]interface{}{
			"id": userID,
		},
	})
	if err != nil {
		return models.User{}, err
	}
	if len(list) < 1 {
		return models.User{}, fmt.Errorf("user storage: GetByID not found")
	}
	return list[0], nil
}

func (us *UserStorage) Delete(ctx context.Context, username string) error {
	err := us.adapter.Delete(ctx, &models.User{}, adapter.Condition{
		Equal: map[string]interface{}{
			"username": username,
		},
	})
	if err != nil {
		return err
	}
	return nil
}

func (us *UserStorage) List(ctx context.Context, condition adapter.Condition) ([]models.User, error) {
	var users []models.User
	err := us.adapter.List(ctx, &users, "users", condition)
	if err != nil {
		return nil, err
	}
	return users, nil
}

func (us *UserStorage) Login(ctx context.Context, username, password string) (string, error) {
	user, err := us.List(ctx, adapter.Condition{
		Equal: map[string]interface{}{
			"username": username,
		},
	})

	if err != nil {
		return "", err
	}

	if user[0].GetPassword() != password {
		return "", fmt.Errorf("invalid credentials")
	}

	_, tokenString, err := TokenAuth.Encode(map[string]interface{}{"username": username, "pass": password})
	if err != nil {
		return "", err
	}
	return tokenString, nil
}

func (us *UserStorage) Logout(ctx context.Context, remote string) error {
	return fmt.Errorf("logout is prohibited for address %s", remote)
}

// это неправильно, нужно в инфраструктуру всё вынести, но хочется уже закончить :)
var TokenAuth = jwtauth.New("HS256", []byte("secret"), nil)
