package controller

import "gitlab/petstore/internal/models"

type ByIDRq struct {
	ID int
}

type ByIDRsp models.Order

type OrderRq models.Order

type OrderRsp models.Order

type ApiResponse struct {
	Code    int    `json:"code"`
	Type    string `json:"type"`
	Message string `json:"message"`
}

type GetInventoryRsp map[string]int
