package controller

import (
	"encoding/json"
	"github.com/go-chi/chi/v5"
	"gitlab/petstore/internal/infrastructure/component"
	"gitlab/petstore/internal/infrastructure/responder"
	"gitlab/petstore/internal/models"
	"gitlab/petstore/internal/modules/store/service"
	"net/http"
	"strconv"
)

type StoreController interface {
	CreateOrder(w http.ResponseWriter, r *http.Request)
	GetOrderById(w http.ResponseWriter, r *http.Request)
	GetInventory(w http.ResponseWriter, r *http.Request)
	DeleteOrder(w http.ResponseWriter, r *http.Request)
}

type Store struct {
	service   service.StoreServicer
	responder responder.Responder
}

func NewStoreCtl(service service.StoreServicer, components *component.Components) *Store {
	return &Store{
		service:   service,
		responder: components.Responder,
	}
}

// CreateOrder
// @Summary Place an order for a pet
// @Tags Store
// @Accept  json
// @Produce  json
// @Param request body OrderRq true "Create order request"
// @Success 200 {object} ApiResponse
// @Failure 400 {object} ApiResponse
// @Router /store/order [post]
func (s Store) CreateOrder(w http.ResponseWriter, r *http.Request) {
	var order OrderRq

	err := json.NewDecoder(r.Body).Decode(&order)
	if err != nil {
		s.responder.ErrorBadRequest(w, err)
		return
	}

	err = s.service.Create(r.Context(), models.Order(order))
	if err != nil {
		s.responder.ErrorInternal(w, err)
		return
	}

	s.responder.OutputJSON(w, ApiResponse{
		Code:    http.StatusOK,
		Type:    "success",
		Message: "order created",
	})
}

// GetOrderById
// @Summary Find purchase order by ID
// @Tags Store
// @Accept  json
// @Produce  json
// @Param orderId path integer true "Get order request"
// @Success 200 {object} OrderRsp
// @Failure 400 {object} ApiResponse
// @Router /store/order/{orderId} [get]
func (s Store) GetOrderById(w http.ResponseWriter, r *http.Request) {
	orderId := chi.URLParam(r, "orderId")
	id, err := strconv.Atoi(orderId)
	if err != nil {
		s.responder.ErrorBadRequest(w, err)
		return
	}
	order, err := s.service.GetByID(r.Context(), id)
	if err != nil {
		s.responder.ErrorInternal(w, err)
		return
	}
	s.responder.OutputJSON(w, order)
}

// GetInventory
// @Summary Returns pet inventories by status
// @Tags Store
// @Accept  json
// @Produce  json
// @Param request path string false "Get inventory request"
// @Success 200 {object} GetInventoryRsp
// @Failure 400 {object} ApiResponse
// @Router /store/inventory [get]
func (s Store) GetInventory(w http.ResponseWriter, r *http.Request) {
	inventory, err := s.service.GetInventory(r.Context())
	if err != nil {
		s.responder.ErrorInternal(w, err)
		return
	}
	s.responder.OutputJSON(w, inventory)
}

// DeleteOrder
// @Summary Delete purchase order by ID
// @Tags Store
// @Accept  json
// @Produce  json
// @Param orderId path int true "Delete order request"
// @Success 200 {object} ApiResponse
// @Failure 400 {object} ApiResponse
// @Router /store/order/{orderId} [delete]
func (s Store) DeleteOrder(w http.ResponseWriter, r *http.Request) {
	orderId := chi.URLParam(r, "orderId")
	id, err := strconv.Atoi(orderId)
	if err != nil {
		s.responder.ErrorBadRequest(w, err)
		return
	}

	err = s.service.Delete(r.Context(), id)
	if err != nil {
		s.responder.ErrorInternal(w, err)
		return
	}

	s.responder.OutputJSON(w, ApiResponse{
		Code:    http.StatusOK,
		Type:    "success",
		Message: "order deleted",
	})
}
