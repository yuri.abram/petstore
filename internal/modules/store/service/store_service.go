package service

import (
	"context"
	"gitlab/petstore/internal/db/adapter"
	"gitlab/petstore/internal/models"
	"gitlab/petstore/internal/modules/store/storage"
	"go.uber.org/zap"
)

type StoreServicer interface {
	GetByID(ctx context.Context, id int) (models.Order, error)
	Create(ctx context.Context, order models.Order) error
	Delete(ctx context.Context, id int) error
	List(ctx context.Context, condition adapter.Condition) ([]models.Order, int, error)
	GetInventory(ctx context.Context) (map[string]int, error)
}

type StoreService struct {
	storage storage.OrderStorager
	logger  *zap.Logger
}

func NewStoreService(storage storage.OrderStorager, logger *zap.Logger) *StoreService {
	return &StoreService{
		storage: storage,
		logger:  logger,
	}
}

// GetInventory не понял что хотят вообще
func (ss *StoreService) GetInventory(ctx context.Context) (map[string]int, error) {
	orders, _, err := ss.storage.List(ctx, adapter.Condition{})
	if err != nil {
		ss.logger.Error("failed to get inventory", zap.Error(err))
		return nil, err
	}

	inventory := make(map[string]int)
	for _, order := range orders {
		inventory[order.Status] += order.Quantity
	}
	return inventory, nil
}

func (ss *StoreService) Delete(ctx context.Context, id int) error {
	return ss.storage.Delete(ctx, id)
}

func (ss *StoreService) List(ctx context.Context, condition adapter.Condition) ([]models.Order, int, error) {
	orders, total, err := ss.storage.List(ctx, condition)
	if err != nil {
		ss.logger.Error("failed to get orders", zap.Error(err))
		return nil, 0, err
	}
	return orders, total, nil
}

func (ss *StoreService) GetByID(ctx context.Context, id int) (models.Order, error) {
	order, err := ss.storage.GetByID(ctx, id)
	if err != nil {
		ss.logger.Error("failed to get order", zap.Error(err))
		return models.Order{}, err
	}
	return order, nil
}

func (ss *StoreService) Create(ctx context.Context, order models.Order) error {
	err := ss.storage.Create(ctx, order)
	if err != nil {
		ss.logger.Error("failed to create order", zap.Error(err))
		return err
	}
	return nil
}
