package storage

import (
	"context"
	sq "github.com/Masterminds/squirrel"
	"gitlab/petstore/internal/db/adapter"
	"gitlab/petstore/internal/infrastructure/db/scanner"
	"gitlab/petstore/internal/models"
)

type OrderStorager interface {
	Create(ctx context.Context, order models.Order) error
	Update(ctx context.Context, order models.Order) error
	Delete(ctx context.Context, orderID int) error
	GetByID(ctx context.Context, orderID int) (models.Order, error)
	List(ctx context.Context, condition adapter.Condition) ([]models.Order, int, error)
}

type Order struct {
	adapter *adapter.SQLAdapter
}

func NewOrderStorage(sqlAdapter *adapter.SQLAdapter) *Order {
	return &Order{adapter: sqlAdapter}
}

func (p *Order) Create(ctx context.Context, order models.Order) error {
	return p.adapter.Create(ctx, &order)
}

func (p *Order) Update(ctx context.Context, order models.Order) error {
	err := p.adapter.Update(ctx, &order, adapter.Condition{
		Equal: sq.Eq{
			"id": order.GetID(),
		},
	}, scanner.Update)
	if err != nil {
		return err
	}
	return nil
}

func (p *Order) Delete(ctx context.Context, orderID int) error {
	order, err := p.GetByID(ctx, orderID)
	if err != nil {
		return err
	}

	return p.adapter.Delete(ctx, &order, adapter.Condition{
		Equal: sq.Eq{
			"id": orderID,
		},
	})
}

func (p *Order) GetByID(ctx context.Context, orderID int) (models.Order, error) {
	var order models.Order
	err := p.adapter.List(ctx, &order, "orders", adapter.Condition{
		Equal: sq.Eq{
			"id": orderID,
		},
	})
	if err != nil {
		return models.Order{}, err
	}
	return order, nil
}

func (p *Order) List(ctx context.Context, condition adapter.Condition) ([]models.Order, int, error) {
	var orders []models.Order

	err := p.adapter.List(ctx, &orders, "orders", condition)
	if err != nil {
		return nil, 0, err
	}

	return orders, len(orders), nil
}
