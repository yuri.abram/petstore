package modules

import (
	"gitlab/petstore/internal/db/adapter"

	pstorage "gitlab/petstore/internal/modules/pet/storage"
	ostorage "gitlab/petstore/internal/modules/store/storage"
	ustorage "gitlab/petstore/internal/modules/user/storage"
)

type Storages struct {
	Pet   pstorage.PetStorager
	Store ostorage.OrderStorager
	Users ustorage.UserRepository
}

func NewStorages(sqlAdapter *adapter.SQLAdapter) *Storages {
	return &Storages{
		Pet:   pstorage.NewPetStorage(sqlAdapter),
		Store: ostorage.NewOrderStorage(sqlAdapter),
		Users: ustorage.NewUserStorage(sqlAdapter),
	}
}
